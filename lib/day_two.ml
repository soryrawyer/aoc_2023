(* day two: snow island *)
(* each line in day2.txt represents an iteration of the cube game *)
(* the format is roughly: *)
(* Game <game id>: <cube list>; <cube list>; <cube list> *)
(* example: *)
(* Game 10: 2 blue, 16 red, 2 green; 1 green, 16 red, 6 blue; 9 red, 3 green; 1 green, 2 blue, 8 red; 8 red, 6 blue, 3 green *)
(* each section in the semicolon-delimited list represents the cubes *)
(* brought out of the bag each time *)
(* given this information, how many games were possible with this cube setup: *)
(* 12 red cubes, 13 green cubes, and 14 blue cubes *)
(* in other words: if any of the games listed have more than this number *)
(* of cubes for the respective color, that game would've been IMPOSSIBLE *)
(* the answer to this part is the sum of the IDs of impossible games *)

(* iterate over lines *)
(* for each line, parse game id *)
(* split remaining string on semicolon *)

open Angstrom
module CubeCounts = Map.Make (String)

let valid_game red green blue = red <= 12 && green <= 13 && blue <= 14

(* these whitespace functions were lifted straight from this intro to Angstrom: *)
(* https://ocamlverse.net/content/monadic-parsers-angstrom.html *)
let is_whitespace = function
  | '\x20' | '\x0a' | '\x0d' | '\x09' -> true
  | _ -> false

let whitespace = take_while is_whitespace
let is_alpha = function 'a' .. 'z' -> true | 'A' .. 'Z' -> true | _ -> false
let word = take_while is_alpha
let is_digit = function '0' .. '9' -> true | _ -> false
let number = take_while1 is_digit

(* when parsing lines: *)
(* 1. expect a word that should be "Game" *)
(* 2. expect an integer followed by a colon *)
(* 3. expect three <integer> <color> sections *)
let parse_game = word *> advance 1 *> number

let get_game_number text =
  match Angstrom.parse_string ~consume:Prefix parse_game text with
  | Ok value -> Some (int_of_string value)
  | Error _ -> None

let get_num_cubes =
  whitespace *> number >>= fun num ->
  whitespace *> word >>= fun color -> return (int_of_string num, color)

let parse_cubes drawing =
  let clues = String.split_on_char ',' drawing in
  let get_cube_pairings text =
    match Angstrom.parse_string ~consume:Prefix get_num_cubes text with
    | Ok (count, color) -> Some (count, color)
    | Error _ -> None
  in
  let filter_nones l = List.filter Option.is_some l in
  let unoption l = List.map Option.get l in
  List.map get_cube_pairings clues |> filter_nones |> unoption

let update_map key value mapping compare =
  match CubeCounts.find_opt key mapping with
  | Some existing ->
      if compare value existing then CubeCounts.add key value mapping
      else mapping
  | None -> CubeCounts.add key value mapping

let get_drawings whole_game_str compare =
  let clues = String.split_on_char ';' whole_game_str in
  (* map over the list of clues and then fold em up in a map *)
  let cube_counts = List.map parse_cubes clues in
  let empty_map = CubeCounts.empty in
  let rec update_map_from_clue acc cube_list =
    match cube_list with
    | [] -> acc
    | (count, color) :: tl ->
        update_map_from_clue (update_map color count acc compare) tl
  in
  List.fold_left update_map_from_clue empty_map cube_counts

let get_count_or_zero mapping key =
  match CubeCounts.find_opt key mapping with Some value -> value | None -> 0

let get_color_counts mapping =
  let red = get_count_or_zero mapping "red" in
  let blue = get_count_or_zero mapping "blue" in
  let green = get_count_or_zero mapping "green" in
  (red, blue, green)

let part_one_line_extract red green blue game =
  if valid_game red green blue then get_game_number game else None

let part_two_line_extract red green blue _ = red * green * blue

let process_line line compare extract =
  match String.split_on_char ':' line with
  | [ game; cube_drawings ] ->
      let counts = get_drawings cube_drawings compare in
      let red, blue, green = get_color_counts counts in
      extract red green blue game
  | _ -> failwith "uh oh!"

let part_one filename =
  let ic = open_in filename in
  let rec read_lines acc =
    match input_line ic with
    | (str : string) -> (
        match process_line str ( > ) part_one_line_extract with
        | Some value -> read_lines (value :: acc)
        | None -> read_lines acc)
    | exception End_of_file -> acc
  in
  read_lines []

let part_two filename =
  let ic = open_in filename in
  let rec read_lines acc =
    match input_line ic with
    | (str : string) ->
        let result = process_line str ( > ) part_two_line_extract in
        read_lines (result :: acc)
    | exception End_of_file -> acc
  in
  read_lines []

let sum l = List.fold_left ( + ) 0 l

let solve =
  let data = "/home/rory/dev/aoc_2023/input/day2.txt" in
  let p1 = part_one data |> sum |> string_of_int in
  let p2 = part_two data |> sum |> string_of_int in
  Printf.sprintf "part one: %s\npart two: %s" p1 p2
