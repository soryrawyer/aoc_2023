val nums_in_line_p1 : string -> string list
val num_of_line : (string -> string list) -> string -> int
val part_one : string -> int
val spelled_out_digit : string -> (string * int) option
val nums_in_line_p2 : string -> string list
val part_two : string -> int
val solve : string
