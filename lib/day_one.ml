(* solution to day 1 of advent of code 2023 *)
(* for each line, get the first and last digit and combine them *)
(* to make a two-digit number, then add them all up *)

let data = "/home/rory/dev/aoc_2023/input/day1.txt"
let is_digit = function '0' .. '9' -> true | _ -> false
let str_of_char c = String.make 1 c

let nums_in_line_p1 line =
  let rec aux l i acc =
    if i = String.length l then acc
    else if is_digit (String.get l i) then
      aux l (i + 1) (str_of_char (String.get l i) :: acc)
    else aux l (i + 1) acc
  in
  aux line 0 []

let num_of_line (nums_in_line : string -> string list) line =
  let nums = nums_in_line line in
  int_of_string
    (String.concat "" [ List.nth nums (List.length nums - 1); List.nth nums 0 ])

let lines ic nums_in_line =
  let rec aux acc chan =
    match input_line chan with
    | (str : string) -> nums_in_line str :: aux acc chan
    | exception End_of_file -> []
  in
  aux [] ic

let sum l = List.fold_left ( + ) 0 l

let part_one filename =
  let ic = open_in filename in
  let num = num_of_line nums_in_line_p1 in
  sum (lines ic num)

(* part two-specific functions *)

(* this really should be trying to pull a number out with a regex *)
(* and then converting it to a number, but this way also works *)
let spelled_out_digit line =
  if String.starts_with ~prefix:"one" line then Some ("1", 3)
  else if String.starts_with ~prefix:"two" line then Some ("2", 2)
  else if String.starts_with ~prefix:"three" line then Some ("3", 5)
  else if String.starts_with ~prefix:"four" line then Some ("4", 4)
  else if String.starts_with ~prefix:"five" line then Some ("5", 4)
  else if String.starts_with ~prefix:"six" line then Some ("6", 3)
  else if String.starts_with ~prefix:"seven" line then Some ("7", 5)
  else if String.starts_with ~prefix:"eight" line then Some ("8", 5)
  else if String.starts_with ~prefix:"nine" line then Some ("9", 4)
  else None

(* numbers can show up in one of two ways: *)
(* 1. the character is a digit *)
(* 2. the character is the first letter of a spelled-out number *)
(* if (2), check the next N characters to see if there's actually *)
(* a number there *)
let nums_in_line_p2 line =
  let rec aux l i acc =
    if i = String.length l then acc
    else if is_digit (String.get l i) then
      aux l (i + 1) (str_of_char (String.get l i) :: acc)
    else
      match spelled_out_digit (String.sub l i (String.length l - i)) with
      | Some (num, offset) -> aux l (i + offset - 1) (num :: acc)
      | None -> aux l (i + 1) acc
  in
  aux line 0 []

let part_two filename =
  let ic = open_in filename in
  let num = num_of_line nums_in_line_p2 in
  sum (lines ic num)

let solve =
  let one = part_one data |> string_of_int in
  let two = part_two data |> string_of_int in
  Printf.sprintf "part one: %s\npart two: %s" one two
