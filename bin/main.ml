let solutions = function
  | "1" -> Aoc_2023.Day_one.solve
  | "2" -> Aoc_2023.Day_two.solve
  | _ -> "no solution for that day yet"

let () =
  let day = Array.get Sys.argv 1 in
  print_endline "aocaml";
  print_endline (solutions day)
