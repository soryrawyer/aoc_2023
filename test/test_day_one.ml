open OUnit2
open Aoc_2023
include Day_one

let ae exp got _test_ctx = assert_equal exp got
let p1_parsing = Day_one.num_of_line Day_one.nums_in_line_p1
let p2_parsing = Day_one.num_of_line Day_one.nums_in_line_p2

let tests =
  [
    "getting a number from a line" >:: ae (p1_parsing "1abc2") 12;
    "handling multiple numbers in a line" >:: ae (p1_parsing "a1b2c3d4e5f") 15;
    "parsing numbers for part two" >:: ae (p2_parsing "two1nine") 29;
    "overlapping numbers in part two" >:: ae (p2_parsing "eightwothree") 83;
    "another overlap" >:: ae (p2_parsing "xtwone3four") 24;
    "mixed numbers and spelled out digits"
    >:: ae (p2_parsing "4nineeightseven2") 42;
    "more test cases" >:: ae (p2_parsing "zoneight234") 14;
    "hmph" >:: ae (p2_parsing "7pqrstsixteen") 76;
  ]
