open OUnit2

let () =
  run_test_tt_main
    ("aoc day one tests" >::: List.append Test_day_one.tests Test_day_two.tests)
