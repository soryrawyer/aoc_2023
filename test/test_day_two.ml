open OUnit2
open Aoc_2023
include Day_two

let ae exp got _test_ctx = assert_equal exp got

let tests =
  [
    "get_game_number can parse numbers correctly"
    >:: ae (Day_two.get_game_number "Game 10: fdsa") (Some 10);
    "parse_cubes can parse cube counts correctly"
    >:: ae (Day_two.parse_cubes " 3 blue, 4 red") [ (3, "blue"); (4, "red") ];
    "process line can spot a valid game"
    >:: ae
          (Day_two.process_line
             "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green" ( > )
             Day_two.part_one_line_extract)
          (Some 1);
    "process line can spot an invalid game"
    >:: ae
          (Day_two.process_line
             "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 \
              green, 1 red"
             ( > ) Day_two.part_one_line_extract)
          None;
    "part two should work?"
    >:: ae
          (Day_two.process_line
             "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green" ( > )
             Day_two.part_two_line_extract)
          48;
  ]
